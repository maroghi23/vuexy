export default [
  {
    header: 'Home',
  },
  {
    title: 'Dashboards',
    route: 'dashboard-ecommerce',
    icon: 'HomeIcon',
  },
]
