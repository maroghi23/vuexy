export default [
    {
        path: '/article', 
        name: 'article', 
        component: () => import('@/views/master/article/Article.vue'),
    },
    {
        path: '/article/edit/:id',
        name: 'edit-article',
        component: () => import('@/views/master/article/Edit.vue'),
    }
]