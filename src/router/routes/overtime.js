export default [
  {
    path: '/overtime',
    name: 'overtime',
    component: () => import('@/views/overtime/Overtime.vue'),
  },
  {
    path: '/overtime/create',
    name: 'overtime-create',
    component: () => import('@/views/overtime/overtime/request/Create.vue'),
  },
]
